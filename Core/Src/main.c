/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <max17261_driver.h>
#include <at86rf215.h>
#include "antenna.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

uint8_t ret;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

CAN_HandleTypeDef hcan1;

I2C_HandleTypeDef hi2c1;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
struct max17261_conf hmax17261;
struct power_status power_status;
struct at86rf215 hat86;
struct antenna_status antenna_status;
uint8_t buffer[256];

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_CAN1_Init(void);
static void MX_I2C1_Init(void);
static void MX_RTC_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_ADC1_Init(void);
/* USER CODE BEGIN PFP */
HAL_StatusTypeDef update_power_status(struct power_status*);
void sidloc_transceiver_enable(uint8_t enable);
void sidloc_fpga_enable(uint8_t enable);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CAN1_Init();
  MX_I2C1_Init();
  MX_RTC_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  MX_ADC1_Init();
  /* USER CODE BEGIN 2 */
  /* Setup MAX17261 */
  hmax17261.DesignCap = BATTERY_CAPACITY;
  hmax17261.IchgTerm = BATTERY_CRG_TERM_I;
  hmax17261.VEmpty = (BATTERY_V_EMPTY << 7) | (BATTERY_V_Recovery & 0x7F);
  hmax17261.R100 = 1;
  hmax17261.ChargeVoltage = POWER_CHG_VOLTAGE;
  hmax17261.force_init = 0;
  ret = max17261_init(&hmax17261);
  sidloc_fpga_enable(GPIO_PIN_SET);
  /* SETUP ATRF */
#ifdef TEST_RADIO
	/* AT86RF215 preperation for TX FSK-2 50kHz */
	hat86.clko_os = AT86RF215_RF_CLKO_26_MHZ;
	hat86.clk_drv = AT86RF215_RF_DRVCLKO2;
	hat86.rf_femode_09 = AT86RF215_RF_FEMODE0;
	hat86.xo_trim = 0;
	hat86.xo_fs = 0;
	hat86.irqp = 0;
	hat86.irqmm = 0;
	hat86.pad_drv = AT86RF215_RF_DRV4;
	sidloc_transceiver_enable(GPIO_PIN_SET);
	int ret = at86rf215_init(&hat86);
	if (ret) {
		Error_Handler();
	}
	struct at86rf215_radio_conf x;
	x.cm = AT86RF215_CM_FINE_RES_04;
	x.lbw = AT86RF215_PLL_LBW_DEFAULT;
	ret = at86rf215_radio_conf(&hat86, AT86RF215_RF09, &x);
	if (ret) {
		Error_Handler();
	}

	ret = at86rf215_set_radio_irq_mask(&hat86, AT86RF215_RF09, 0xFE);
	if (ret) {
		Error_Handler();
	}

	at86rf215_set_bbc_irq_mask(&hat86, AT86RF215_RF09, 0xFF);

	ret = at86rf215_set_freq(&hat86, AT86RF215_RF09, 403000000);
	if (ret) {
		Error_Handler();
	}
	ret = at86rf215_set_pac(&hat86, AT86RF215_RF09, AT86RF215_PACUR_NO_RED, TX_PWR_REG);
	if (ret) {
		Error_Handler();
	}

	struct at86rf215_bb_conf bb;
	bb.ctx = 0;
	bb.fcsfe = 0;
	bb.txafcs = 1;
	bb.fcst = AT86RF215_FCS_16;
	bb.pt = AT86RF215_BB_MRFSK;
	bb.fsk.mord = AT86RF215_2FSK;
	bb.fsk.midx = AT86RF215_MIDX_3;
	bb.fsk.midxs = AT86RF215_MIDXS_88;
	bb.fsk.bt = AT86RF215_FSK_BT_10;
	bb.fsk.srate = AT86RF215_FSK_SRATE_400;
	bb.fsk.fi = 0;
	bb.fsk.preamble_length = 256;
	bb.fsk.rxo = AT86RF215_FSK_RXO_DISABLED;
	bb.fsk.pdtm = 0;
	bb.fsk.rxpto = 1;
	bb.fsk.mse = 0;
	bb.fsk.pri = 0;
	bb.fsk.fecs = AT86RF215_FSK_FEC_RSC;
	bb.fsk.fecie = 0;
	bb.fsk.sfd_threshold = 15;
	bb.fsk.preamble_threshold = 8;
	bb.fsk.sfdq = 1;
	bb.fsk.sfd32 = 1;
	bb.fsk.rawrbit = 0;
	bb.fsk.csfd1 = AT86RF215_SFD_UNCODED_IEEE;
	bb.fsk.csfd0 = AT86RF215_SFD_UNCODED_IEEE;
	bb.fsk.preamble_length = 64;
	bb.fsk.sfd = 0;
	bb.fsk.dw = 0;
	bb.fsk.preemphasis = 0;
	bb.fsk.dm = 1;

	ret = at86rf215_bb_conf(&hat86, AT86RF215_RF09, &bb);
	if (ret) {
		Error_Handler();
	}

	HAL_Delay(1000);

	ret = at86rf215_set_cmd(&hat86, AT86RF215_CMD_RF_TXPREP, AT86RF215_RF09);
	if (ret) {
		Error_Handler();
	}

	HAL_Delay(1000);

	at86rf215_pll_ls_t pll_ls;
	ret = at86rf215_get_pll_ls(&hat86, &pll_ls, AT86RF215_RF09);
	if (ret) {
		Error_Handler();
	}

	at86rf215_rf_state_t s;
	ret = at86rf215_get_state(&hat86, &s, AT86RF215_RF09);
	if (ret) {
		Error_Handler();
	}

	ret = at86rf215_bb_enable(&hat86, AT86RF215_RF09, 1);
	if (ret) {
		Error_Handler();
	}
#endif
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	update_power_status(&power_status);
  // Set TM pin to indicate proper powerup
  HAL_GPIO_WritePin(TM_GPIO_Port, TM_Pin, GPIO_PIN_SET);
  // Delay 30s for antenna deployment
  HAL_Delay(ANT_PRE_DEPLOY_DELAY);
  antenna_deploy();

  while (1) {
	  HAL_Delay(200);
#ifdef TEST_RADIO
//	  Enable RFFE
	  HAL_GPIO_WritePin(RFFE_EN_GPIO_Port, RFFE_EN_Pin, SET);
	  HAL_GPIO_WritePin(RFFE_TX_EN_GPIO_Port, RFFE_TX_EN_Pin, SET);
	  HAL_Delay(100);
	  update_power_status(&power_status);
	  ret = at86rf215_tx_frame(&hat86, AT86RF215_RF09, buffer, 256);
	  if (ret) {
		  Error_Handler();
	  }
	  HAL_Delay(100);
	  HAL_GPIO_WritePin(RFFE_TX_EN_GPIO_Port, RFFE_TX_EN_Pin, RESET);
	  HAL_GPIO_WritePin(RFFE_EN_GPIO_Port, RFFE_EN_Pin, RESET);
#endif
	  update_power_status(&power_status);
	  antenna_status.status = HAL_GPIO_ReadPin(ANT_SENSE_GPIO_Port, ANT_SENSE_Pin);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_RCC_MCOConfig(RCC_MCO1, RCC_MCO1SOURCE_SYSCLK, RCC_MCODIV_1);
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_5;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief CAN1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN1_Init(void)
{

  /* USER CODE BEGIN CAN1_Init 0 */

  /* USER CODE END CAN1_Init 0 */

  /* USER CODE BEGIN CAN1_Init 1 */

  /* USER CODE END CAN1_Init 1 */
  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 16;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan1.Init.TimeSeg1 = CAN_BS1_1TQ;
  hcan1.Init.TimeSeg2 = CAN_BS2_1TQ;
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = DISABLE;
  hcan1.Init.AutoWakeUp = DISABLE;
  hcan1.Init.AutoRetransmission = DISABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN1_Init 2 */

  /* USER CODE END CAN1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00303D5B;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */

  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(RCC_OSC_EN_GPIO_Port, RCC_OSC_EN_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(TRX_RST_GPIO_Port, TRX_RST_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(FPGA_CS_GPIO_Port, FPGA_CS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, RFFE_EN_Pin|CAN_SHDN_Pin|CAN_STB_Pin|ANT_DEPLOY_Pin
                          |TRX_EN_Pin|RFFE_TX_EN_Pin|FPGA_EN_Pin|CHG_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(TRX_CS_GPIO_Port, TRX_CS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(TM_GPIO_Port, TM_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : RCC_OSC_EN_Pin */
  GPIO_InitStruct.Pin = RCC_OSC_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(RCC_OSC_EN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : FPGA_INIT_Pin */
  GPIO_InitStruct.Pin = FPGA_INIT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(FPGA_INIT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : TRX_RST_Pin FPGA_CS_Pin */
  GPIO_InitStruct.Pin = TRX_RST_Pin|FPGA_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PSU_PG_Pin */
  GPIO_InitStruct.Pin = PSU_PG_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(PSU_PG_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : ANT_SENSE_Pin */
  GPIO_InitStruct.Pin = ANT_SENSE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(ANT_SENSE_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RFFE_EN_Pin CAN_SHDN_Pin CAN_STB_Pin ANT_DEPLOY_Pin
                           TRX_EN_Pin RFFE_TX_EN_Pin FPGA_EN_Pin TRX_CS_Pin */
  GPIO_InitStruct.Pin = RFFE_EN_Pin|CAN_SHDN_Pin|CAN_STB_Pin|ANT_DEPLOY_Pin
                          |TRX_EN_Pin|RFFE_TX_EN_Pin|FPGA_EN_Pin|TRX_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PSU_BAT_ALERT_Pin FPGA_DONE_Pin */
  GPIO_InitStruct.Pin = PSU_BAT_ALERT_Pin|FPGA_DONE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PA8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : TRX_IRQ_Pin */
  GPIO_InitStruct.Pin = TRX_IRQ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(TRX_IRQ_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : TM_Pin */
  GPIO_InitStruct.Pin = TM_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(TM_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : CHG_EN_Pin */
  GPIO_InitStruct.Pin = CHG_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CHG_EN_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */
uint8_t update_power_status(struct power_status *power_status) {
	HAL_StatusTypeDef ret = HAL_OK;
	power_status->voltage = max17261_get_voltage(&hmax17261);
	power_status->voltage_avg = max17261_get_average_voltage(&hmax17261);
	power_status->current = max17261_get_current(&hmax17261);
	power_status->current_avg = max17261_get_average_current(&hmax17261);
	power_status->SOC = max17261_get_SOC(&hmax17261);
	power_status->RepCap = max17261_get_reported_capacity(&hmax17261);
	power_status->FullRepCap = max17261_get_full_reported_capacity(&hmax17261);
	power_status->temperature = max17261_get_temperature(&hmax17261);
	power_status->temperature_die = max17261_get_die_temperature(&hmax17261);
	power_status->temperature_avg = max17261_get_average_temperature(
			&hmax17261);
	power_status->TTE = max17261_get_TTE(&hmax17261);

	max17261_get_minmax_voltage(&hmax17261, &power_status->voltage_min,
			&power_status->voltage_max);
	max17261_get_minmax_current(&hmax17261, &power_status->current_min,
			&power_status->current_max);
	max17261_get_minmax_temperature(&hmax17261, &power_status->temperature_min,
			&power_status->temperature_max);
	max17261_get_learned_params(&hmax17261);
	ret |= max17261_get_qrtable_values(&hmax17261);
	return ret;
}

void sidloc_transceiver_enable(uint8_t enable) {
	HAL_GPIO_WritePin(TRX_EN_GPIO_Port, TRX_EN_Pin, enable);
}

void sidloc_fpga_enable(uint8_t enable) {
	HAL_GPIO_WritePin(FPGA_EN_GPIO_Port, FPGA_EN_Pin, enable);
}

int at86rf215_set_rstn(uint8_t enable)
{
	HAL_GPIO_WritePin(TRX_RST_GPIO_Port, TRX_RST_Pin, enable & 0x1);
	return AT86RF215_OK;
}

void at86rf215_delay_us(uint32_t us)
{
	HAL_Delay(us);
}

int at86rf215_spi_read(uint8_t *out, const uint8_t *in, size_t len)
{
	uint8_t ret;
	HAL_GPIO_WritePin(TRX_CS_GPIO_Port, TRX_CS_Pin, GPIO_PIN_RESET);
	ret = HAL_SPI_TransmitReceive(&hspi1, in, out, len, 1000);
	HAL_GPIO_WritePin(TRX_CS_GPIO_Port, TRX_CS_Pin, GPIO_PIN_SET);
	return ret;
}

int at86rf215_spi_write(const uint8_t *in, size_t len)
{
	uint8_t ret;
	HAL_GPIO_WritePin(TRX_CS_GPIO_Port, TRX_CS_Pin, GPIO_PIN_RESET);
	ret = HAL_SPI_Transmit(&hspi1, in, len, 1000);
	HAL_GPIO_WritePin(TRX_CS_GPIO_Port, TRX_CS_Pin, GPIO_PIN_SET);
	return ret;
}

int at86rf215_irq_enable(uint8_t enable)
{
	if (enable) {
		HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
	} else {
		HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
	}
	return AT86RF215_OK;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)

{

	if (GPIO_Pin == TRX_IRQ_Pin) {

		at86rf215_irq_callback(&hat86);

	}

}

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
