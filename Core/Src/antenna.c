/*
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "antenna.h"

/**
 * @brief Antenna deployment power control
 * Controls power (On/Off) to the antenna release mechanism
 * @param state ant_deploy_power_t value
 */
void
antenna_deploy_power(ant_deploy_power_t state)
{
	HAL_GPIO_WritePin(ANT_DEPLOY_GPIO_Port, ANT_DEPLOY_Pin, state);
}

/**
 * @brief Antenna deployment status
 * Provides information whether the antenna is deployed or stowed.
 * Performs multiple reading to ensure the correct reading is provided
 * @return ant_deploy_status_t value
 */
ant_deploy_status_t
antenna_deploy_status()
{
	uint8_t ret = 0;
	uint8_t on = 0;
	uint8_t off = 0;
	for (uint8_t cnt = 0; cnt < ANT_STATUS_DEBOUNCE_CNT; cnt++) {
		ret = HAL_GPIO_ReadPin(ANT_SENSE_GPIO_Port, ANT_SENSE_Pin);
		if (ret) {
			on++;
		} else {
			off++;
		}
		HAL_Delay(10);
	}
	if (on > ANT_STATUS_DEBOUNCE_CNT * 3 / 4) {
		return ANT_DEPLOYED;
	} else if (off > ANT_STATUS_DEBOUNCE_CNT * 3 / 4) {
		return ANT_STOWED;
	} else {
		return ANT_STOWED;
	}
}

/**
 * @brief Antenna deploy sequence activation
 * Starts the antenna deploy activation sequence. Current is applied on the deploy resistors until
 * either antenna deploy status is changed or timeout occurs
 *
 * @param hwdg the watchdog structure
 * @param wdgid the watchdog id
 * @return antenna deployed status
 */
uint8_t
antenna_deploy()
{
	uint16_t cntr = 0;
	uint32_t start_uptime = 0;

	ant_deploy_status_t status = antenna_deploy_status();

	antenna_deploy_power(ANT_DEPLOY_ON);
	HAL_Delay(ANT_DEPLOY_TIME * 1000);
	antenna_deploy_power(ANT_DEPLOY_OFF);
	status = ANT_ASSUMED_DEPLOYED;

	return status;
}
