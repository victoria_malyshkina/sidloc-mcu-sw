/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
struct power_status {
	uint16_t voltage; 			/**< Voltage in mV */
	uint16_t voltage_avg; 		/**< Average Voltage in mV */
	uint16_t voltage_max; 		/**< Max Voltage in mV */
	uint16_t voltage_min; 		/**< Min Voltage in mV */
	int16_t current; 			/**< Current in mA */
	int16_t current_avg; 		/**< Average Current in mA */
	int16_t current_max; 		/**< Max in mA */
	int16_t current_min; 		/**< Min in mA */
	int8_t temperature_die; 	/**< Current in mA */
	int8_t temperature; 		/**< Current in mA */
	int8_t temperature_avg; 	/**< Average Current in mA */
	int8_t temperature_max; 	/**< Max in mA */
	int8_t temperature_min; 	/**< Min in mA */
	int8_t SOC; 				/**< State of charge in % */
	int16_t FullRepCap;			/**< new full-capacity value  */
	int16_t RepCap;				/**< reported remaining capacity in mAh. */
	uint16_t TTE; 				/**< Time to empty in minutes */
};

typedef enum {
	ANT_STATUS_STOWED,
	ANT_STATUS_DEPLOYED
} sidloc_antenna_state;

struct antenna_status {
	uint8_t deploy;
	sidloc_antenna_state status;
};
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#define TEST_RADIO
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define RCC_OSC_EN_Pin GPIO_PIN_13
#define RCC_OSC_EN_GPIO_Port GPIOC
#define FPGA_INIT_Pin GPIO_PIN_1
#define FPGA_INIT_GPIO_Port GPIOH
#define TRX_RST_Pin GPIO_PIN_1
#define TRX_RST_GPIO_Port GPIOA
#define PSU_PG_Pin GPIO_PIN_2
#define PSU_PG_GPIO_Port GPIOA
#define ANT_SENSE_Pin GPIO_PIN_3
#define ANT_SENSE_GPIO_Port GPIOA
#define FPGA_CS_Pin GPIO_PIN_4
#define FPGA_CS_GPIO_Port GPIOA
#define RFFE_EN_Pin GPIO_PIN_0
#define RFFE_EN_GPIO_Port GPIOB
#define CAN_SHDN_Pin GPIO_PIN_1
#define CAN_SHDN_GPIO_Port GPIOB
#define CAN_STB_Pin GPIO_PIN_2
#define CAN_STB_GPIO_Port GPIOB
#define ANT_DEPLOY_Pin GPIO_PIN_10
#define ANT_DEPLOY_GPIO_Port GPIOB
#define PSU_BAT_ALERT_Pin GPIO_PIN_11
#define PSU_BAT_ALERT_GPIO_Port GPIOB
#define TRX_EN_Pin GPIO_PIN_12
#define TRX_EN_GPIO_Port GPIOB
#define RFFE_TX_EN_Pin GPIO_PIN_13
#define RFFE_TX_EN_GPIO_Port GPIOB
#define FPGA_EN_Pin GPIO_PIN_14
#define FPGA_EN_GPIO_Port GPIOB
#define TRX_CS_Pin GPIO_PIN_15
#define TRX_CS_GPIO_Port GPIOB
#define TRX_IRQ_Pin GPIO_PIN_5
#define TRX_IRQ_GPIO_Port GPIOB
#define TRX_IRQ_EXTI_IRQn EXTI9_5_IRQn
#define TM_Pin GPIO_PIN_3
#define TM_GPIO_Port GPIOH
#define FPGA_DONE_Pin GPIO_PIN_8
#define FPGA_DONE_GPIO_Port GPIOB
#define CHG_EN_Pin GPIO_PIN_9
#define CHG_EN_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */
#define TX_PWR_REG 21
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
