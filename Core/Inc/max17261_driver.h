/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INC_MAX17261_DRIVER_H_
#define INC_MAX17261_DRIVER_H_

#include <max17261.h>
/** Battery capacity in mAh */
#define	BATTERY_CAPACITY	1000
/** Charge termination current in mA */
#define BATTERY_CRG_TERM_I	25
#define BATTERY_V_EMPTY		(3300 / 10)
#define BATTERY_V_Recovery	(3880 / 40)
#define POWER_CHG_VOLTAGE	4200
#endif /* INC_MAX17261_DRIVER_H_ */
