# SIDLOC MCU Software



## Development Guide
SIDLOC MCU software in independent of any kind of development tool.
You can use the development environment of your choice.
In addition the project contains a [Makefile](Makefile) that can be used to
build the firmware automatically.

However we recommend the usage of the TrueSTUDIO or STM32CubeIDE for the
development, debugging and experimentation.
These IDEs have been optimized for the STM32 MCUs and provide helpful
graphical debugging tools.
More information regarding the import process of the project into these tools
can be found at [Import into the TrueSTUDIO or STM32CubeIDE](#import-into-the-truestudio-or-stm32cubeide) section.

### Requirements
* GNU Make
* cross-arm-none-eabi-gcc (>= 5.0)
* STM32 CubeMX (for modifying peripherals and FreeRTOS parameters)

### Dependencies
The SIDLOC MCU codebase depends on the [MAX7261 Driver](https://gitlab.com/librespacefoundation/qubik/max17261-driver) which is shipped as git submodule within the project.

### Import into STM32CubeIDE
- Download STM32CubeIDE
- Open the `sidloc-mcu-sw` directory and delete any possible instances of project configuration files (e.g `.project` or `.cproject` and such)
- Launch STM32CubeIDE and go to File -> Import -> Projects from Folder or Archive and find the source directory of sidloc-mcu-sw
- Open the `sidloc-mcu-sw.ioc` file. This will launch the Device Configuration Tool perspective
- Go to Project -> Generate Code. This will generate the necessary configuration files

Now some final steps are needed:

- Go to Project Properties. Expand the `C/C++ General` Tab. Go to `Paths and Symbols` and add the following directories from workspace: `Drivers/MAX17261`

<!-- ## Documentation
[Doxygen Page](https://librespacefoundation.gitlab.io/sidloc/sidloc-mcu-sw/) -->

<!-- ## Binary Downloads

* [Latest development release](https://gitlab.com/librespacefoundation/sidloc/sidloc-mcu-sw/-/jobs/artifacts/master/download?job=archive_devel) -->

## Website and Contact
For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).

## License
![SIDLOC Logo](docs/assets/sidloc-logo-icon.png)
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png)  
&copy; 2022 [Libre Space Foundation](https://libre.space).
